(ag_percept (percept_pobj e1) (percept_pname isa) (percept_pval eveniment))

(ag_percept (percept_pobj drum1) (percept_pname isa) (percept_pval drum))
(ag_percept (percept_pobj drum2) (percept_pname isa) (percept_pval drum))
(ag_percept (percept_pobj intersectie) (percept_pname isa) (percept_pval intersectie))


(ag_percept (percept_pobj drum1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj drum2) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj intersectie) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj intersectie_center) (percept_pname partof) (percept_pval intersectie))

(ag_percept (percept_pobj drum1) (percept_pname partof) (percept_pval intersectie))
(ag_percept (percept_pobj drum2) (percept_pname partof) (percept_pval intersectie))
(ag_percept (percept_pobj drum1) (percept_pname direction) (percept_pval vertical))
(ag_percept (percept_pobj drum2) (percept_pname direction) (percept_pval orizontal))

(ag_percept (percept_pobj ind1) (percept_pname isa) (percept_pval indicator))
(ag_percept (percept_pobj ind1) (percept_pname partof) (percept_pval drum1))
(ag_percept (percept_pobj ind1) (percept_pname type) (percept_pval drum_prioritar))
(ag_percept (percept_pobj ind1) (percept_pname direction) (percept_pval inainte))


(ag_percept (percept_pobj ind2) (percept_pname isa) (percept_pval indicator))
(ag_percept (percept_pobj ind2) (percept_pname partof) (percept_pval drum2))
(ag_percept (percept_pobj ind2) (percept_pname type) (percept_pval cedeaza))
(ag_percept (percept_pobj ind2) (percept_pname direction) (percept_pval inainte))

(ag_percept (percept_pobj masina1) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj masina1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj masina1) (percept_pname on) (percept_pval drum1))
(ag_percept (percept_pobj masina1) (percept_pname intention) (percept_pval inainte))

(ag_percept (percept_pobj masina2) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj masina2) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj masina2) (percept_pname on) (percept_pval intersectie_center))
(ag_percept (percept_pobj masina2) (percept_pname intention) (percept_pval inainte))



(ag_percept (percept_pobj distanta_intersectie_masina1) (percept_pname distanta) (percept_pval 0))
(ag_percept (percept_pobj distanta_intersectie_masina2) (percept_pname distanta) (percept_pval 10))


















