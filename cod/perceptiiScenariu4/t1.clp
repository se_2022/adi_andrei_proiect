(ag_percept (percept_pobj e1) (percept_pname isa) (percept_pval eveniment))

(ag_percept (percept_pobj banda1) (percept_pname isa) (percept_pval banda))
(ag_percept (percept_pobj banda1) (percept_pname partof) (percept_pval e1))

(ag_percept (percept_pobj banda2) (percept_pname isa) (percept_pval banda))
(ag_percept (percept_pobj banda2) (percept_pname partof) (percept_pval e1))

(ag_percept (percept_pobj trotuar1) (percept_pname isa) (percept_pval trotuar))
(ag_percept (percept_pobj trotuar1) (percept_pname partof) (percept_pval e1))


(ag_percept (percept_pobj masina1) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj masina1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj masina1) (percept_pname direction) (percept_pval inainte))
(ag_percept (percept_pobj masina1) (percept_pname state) (percept_pval inmers))
(ag_percept (percept_pobj masina1) (percept_pname on) (percept_pval banda1))


(ag_percept (percept_pobj masina2) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj masina2) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj masina2) (percept_pname direction) (percept_pval inainte))
(ag_percept (percept_pobj masina2) (percept_pname state) (percept_pval inmers))
(ag_percept (percept_pobj masina2) (percept_pname on) (percept_pval banda1))


(ag_percept (percept_pobj balta1) (percept_pname isa) (percept_pval balta))
(ag_percept (percept_pobj balta1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj balta1) (percept_pname on) (percept_pval banda1))

(ag_percept (percept_pobj persoana1) (percept_pname isa) (percept_pval persoana))
(ag_percept (percept_pobj persoana1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj persoana1) (percept_pname on) (percept_pval trotuar))
(ag_percept (percept_pobj persoana1) (percept_pname near) (percept_pval balta))

(ag_percept (percept_pobj distanta_balta) (percept_pname distanta) (percept_pval 100))
(ag_percept (percept_pobj distanta_masina2) (percept_pname distanta) (percept_pval 20))

(ag_percept (percept_pobj masina1) (percept_pname position) (percept_pval in_back_of_masina2))
(ag_percept (percept_pobj masina1) (percept_pname position) (percept_pval in_front_of_balta))

(ag_percept (percept_pobj distanta_balta) (percept_pname isa) (percept_pval distanta_fata_de_balta))
(ag_percept (percept_pobj distanta_masina2) (percept_pname isa) (percept_pval distanta_fata_de_masina2))

(ag_percept (percept_pobj distanta_masina2) (percept_pname partof) (percept_pval masina1))
(ag_percept (percept_pobj distanta_balta) (percept_pname partof) (percept_pval masina1))

















