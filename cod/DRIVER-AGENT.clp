;
;-------Auxiliary facts ---------------------------------------
;


;;----------------------------------
;;
;;    1. Parcare eleganta [Scenariu 1]
;;
;;----------------------------------

(defrule AGENT::initCycle-parking
    (declare (salience 99))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-parking-maneuver prohibited by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname parking-maneuver) (bel_pval prohibited))) 
)

(defrule AGENT::validate-parking-maneuver
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname parking-maneuver) (bel_pval prohibited))
   
    (ag_bel (bel_type moment) (bel_pobj distanta_spate) (bel_pname distanta) (bel_pval ?dspate))
    (ag_bel (bel_type moment) (bel_pobj distanta_stanga) (bel_pname distanta) (bel_pval ?dstanga))
    (ag_bel (bel_type moment) (bel_pobj distanta_dreapta) (bel_pname distanta) (bel_pval ?ddreapta))
    (test (and (<= ?dspate 0.5) (<= ?dstanga 0.5) (<= ?ddreapta 0.5)))
=>

    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-parking-maneuver NU->DA (nu avem restrictii) " crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname parking-maneuver) (bel_pval allowed))) 
)


;;----------------------------------
;;
;;    2. Intersectie [Scenariu 2]
;;
;;----------------------------------

(defrule AGENT::initCycle-intersection
    (declare (salience 99))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-move-the-car maneuver allowed by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname move-the-car-maneuver) (bel_pval allowed))) 
)

(defrule AGENT::validate-move-the-car-maneuver
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname move-the-car-maneuver) (bel_pval allowed))
   
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname isa) (bel_pval masina))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname partof) (bel_pval e1))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname direction) (bel_pval inainte))	
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname state) (bel_pval oprita))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname proximity) (bel_pval ?pm1))

    (ag_bel (bel_type moment) (bel_pobj ?d1) (bel_pname isa) (bel_pval drum))
    (ag_bel (bel_type moment) (bel_pobj ?d1) (bel_pname direction) (bel_pval transversal))
    (ag_bel (bel_type moment) (bel_pobj ?d1) (bel_pname proximity) (bel_pval ?pd1))

    (ag_bel (bel_type moment) (bel_pobj ?ind1) (bel_pname isa) (bel_pval indicator))
    (ag_bel (bel_type moment) (bel_pobj ?ind1) (bel_pname type) (bel_pval drum_prioritar))
    (ag_bel (bel_type moment) (bel_pobj ?ind1) (bel_pname proximity) (bel_pval ?pi1))

    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname isa) (bel_pval masina))
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname on) (bel_pval ?dm1))
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname intention) (bel_pval ?inte))

    (test (and (< ?pi1 ?pd1) (< ?pd1 ?pm1) (eq ?inte inainte) (eq ?d1 ?dm1)))
=>

    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-move-the-car-maneuver NU->DA (nu avem restrictii) " crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname move-the-car-maneuver) (bel_pval prohibited))) 
)


;;----------------------------------
;;
;;    3. Permitere depasire [Scenariu 3]
;;
;;----------------------------------

(defrule AGENT::initCycle-slide-right
    (declare (salience 99))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-slide-right maneuver prohibited by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname slide-right-maneuver) (bel_pval prohibited))) 
)

(defrule AGENT::validate-slide-right-maneuver
    (declare (salience -10))
    ?f <- (ag_bel (bel_type moment) (bel_pname slide-right-maneuver) (bel_pval prohibited))

    (ag_bel (bel_type moment) (bel_pobj ?d1) (bel_pname isa) (bel_pval drum))
    (ag_bel (bel_type moment) (bel_pobj ?d1) (bel_pname direction) (bel_pval ahead))
    (ag_bel (bel_type moment) (bel_pobj ?d1) (bel_pname partof) (bel_pval e1))
   
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname isa) (bel_pval vehicul))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname partof) (bel_pval e1))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname on) (bel_pval ?dr1))	
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname sens) (bel_pval contrasens))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname proximity) (bel_pval ?p1))
    (ag_bel (bel_type moment) (bel_pobj ?m1) (bel_pname state) (bel_pval incoloana))

    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname isa) (bel_pval vehicul))
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname partof) (bel_pval e1))
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname on) (bel_pval ?dr2))	
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname sens) (bel_pval contrasens))
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname proximity) (bel_pval ?p2))
    (ag_bel (bel_type moment) (bel_pobj ?m2) (bel_pname state) (bel_pval indepasire))

    (test (and (eq ?d1 ?dr1) (eq ?d1 ?dr2) (< ?p1 ?p2)))
=>

    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-slide-right-maneuver NU->DA (nu avem restrictii) " crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname slide-right-maneuver) (bel_pval allowed))) 
)


;;----------------------------------
;;
;;    4. breaking-avoid-splash [Scenariu 4]
;;
;;----------------------------------

(defrule AGENT::initCycle-breaking-avoid-splash
    (declare (salience 99))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-breaking-avoid-splash prohibited by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname breaking-overcome-maneuver) (bel_pval prohibited))
	

) 
)

(defrule AGENT::validate-breaking-overcome-maneuver
    (declare (salience -10))
    ?f <-  (ag_bel (bel_type moment) (bel_pname breaking-overcome-maneuver) (bel_pval prohibited))
   
    (ag_bel (bel_type moment) (bel_pobj distanta_balta) (bel_pname distanta) (bel_pval ?dbalta))
    (ag_bel (bel_type moment) (bel_pobj distanta_masina2) (bel_pname distanta) (bel_pval ?dmasina2))
    (ag_bel (bel_type moment) (bel_pobj masina1) (bel_pname position) (bel_pval in_front_of_balta))
    (test (and (<= ?dbalta 70) (<= ?dmasina2 50)))
=>

    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-breaking-maneuver NU->DA (nu avem restrictii) " crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname breaking-overcome-maneuver) (bel_pval breaking))) 
)

(defrule AGENT::validate-breaking-overcome-maneuver1
    (declare (salience -10))
    ?f <-  (ag_bel (bel_type moment) (bel_pname breaking-overcome-maneuver) (bel_pval prohibited))
   
    (ag_bel (bel_type moment) (bel_pobj distanta_balta) (bel_pname distanta) (bel_pval ?dbalta))
    (ag_bel (bel_type moment) (bel_pobj distanta_masina2) (bel_pname distanta) (bel_pval ?dmasina2))
    (ag_bel (bel_type moment) (bel_pobj masina1) (bel_pname position) (bel_pval in_front_of_balta))
(ag_bel (bel_type moment) (bel_pobj masina1) (bel_pname position) (bel_pval in_back_of_masina2))
    (test (and (<= ?dbalta 20) (>= ?dmasina2 50)))
=>

    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-breaking-maneuver NU->DA (nu avem restrictii) " crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname breaking-overcome-maneuver) (bel_pval overcome))) 
)

;;----------------------------------
;;
;;    5. intersection-priority [Scenariu 5]
;;
;;----------------------------------
W
(defrule AGENT::initCycle-intersection-priority
    (declare (salience 99))
    (timp (valoare ?)) 
=>
    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>initCycle-intersection-priority prohibited by default " crlf))
    (assert (ag_bel (bel_type moment) (bel_pname intersection-priority-maneuver) (bel_pval go))
	

) 
)

(defrule AGENT::validate-intersection-priority
    (declare (salience -10))
    ?f <-  (ag_bel (bel_type moment) (bel_pname intersection-priority-maneuver) (bel_pval go))

    (ag_bel (bel_type moment) (bel_pobj distanta_intersectie_masina1) (bel_pname distanta) (bel_pval ?dmasina1))
    (ag_bel (bel_type moment) (bel_pobj distanta_intersectie_masina2) (bel_pname distanta) (bel_pval ?dmasina2))
    (ag_bel (bel_type moment) (bel_pobj masina2) (bel_pname on) (bel_pval intersectie_center))
    (test (and (= ?dmasina1 0) (>= ?dmasina2 0)))
=>

    (if (eq ?*ag-in-debug* TRUE) then (printout t "    <D>validate-breaking-maneuver NU->DA (nu avem restrictii) " crlf))
    (retract ?f)
    (assert (ag_bel (bel_type moment) (bel_pname intersection-priority-maneuver) (bel_pval stop))) 
  
	
)




