(ag_percept (percept_pobj e1) (percept_pname isa) (percept_pval eveniment))
(ag_percept (percept_pobj loc1) (percept_pname isa) (percept_pval loc_parcare))
(ag_percept (percept_pobj loc2) (percept_pname isa) (percept_pval loc_parcare))

(ag_percept (percept_pobj loc1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj loc2) (percept_pname partof) (percept_pval e1))

(ag_percept (percept_pobj loc1) (percept_pname availability) (percept_pval empty))
(ag_percept (percept_pobj loc2) (percept_pname availability) (percept_pval occupied))

(ag_percept (percept_pobj car) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj car) (percept_pname position) (percept_pval center-right))
(ag_percept (percept_pobj distanta_fata) (percept_pname isa) (percept_pval distanta_linie_marcaj))
(ag_percept (percept_pobj distanta_spate) (percept_pname isa) (percept_pval distanta_linie_marcaj))
(ag_percept (percept_pobj distanta_stanga) (percept_pname isa) (percept_pval distanta_laterala_linie_marcaj))
(ag_percept (percept_pobj distanta_dreapta) (percept_pname isa) (percept_pval distanta_laterala_linie_marcaj))

(ag_percept (percept_pobj distanta_fata) (percept_pname partof) (percept_pval car))
(ag_percept (percept_pobj distanta_spate) (percept_pname partof) (percept_pval car))
(ag_percept (percept_pobj distanta_stanga) (percept_pname partof) (percept_pval car))
(ag_percept (percept_pobj distanta_dreapta) (percept_pname partof) (percept_pval car))

(ag_percept (percept_pobj distanta_fata) (percept_pname distanta) (percept_pval 2))
(ag_percept (percept_pobj distanta_spate) (percept_pname distanta) (percept_pval 0.5))
(ag_percept (percept_pobj distanta_stanga) (percept_pname distanta) (percept_pval 0.5))
(ag_percept (percept_pobj distanta_dreapta) (percept_pname distanta) (percept_pval 0.5))

(ag_percept (percept_pobj latime) (percept_pname isa) (percept_pval dimensiune))
(ag_percept (percept_pobj lungime) (percept_pname isa) (percept_pval dimensiune))

(ag_percept (percept_pobj latime) (percept_pname partof) (percept_pval car))
(ag_percept (percept_pobj lungime) (percept_pname partof) (percept_pval car))

(ag_percept (percept_pobj latime) (percept_pname cantitate) (percept_pval 1.5))
(ag_percept (percept_pobj lungime) (percept_pname cantitate) (percept_pval 2.5))

(ag_percept (percept_pobj latime_loc) (percept_pname isa) (percept_pval dimensiune))
(ag_percept (percept_pobj lungime_loc) (percept_pname isa) (percept_pval dimensiune))

(ag_percept (percept_pobj latime_loc) (percept_pname partof) (percept_pval loc1))
(ag_percept (percept_pobj lungime_loc) (percept_pname partof) (percept_pval loc1))

(ag_percept (percept_pobj latime_loc) (percept_pname cantitate) (percept_pval 2.5))
(ag_percept (percept_pobj lungime_loc) (percept_pname cantitate) (percept_pval 5))





