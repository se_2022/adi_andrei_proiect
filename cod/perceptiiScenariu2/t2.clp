(ag_percept (percept_pobj e1) (percept_pname isa) (percept_pval eveniment))


(ag_percept (percept_pobj masina1) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj masina1) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj masina1) (percept_pname direction) (percept_pval inainte))
(ag_percept (percept_pobj masina1) (percept_pname state) (percept_pval oprita))
(ag_percept (percept_pobj masina1) (percept_pname proximity) (percept_pval 2))


(ag_percept (percept_pobj ind1) (percept_pname isa) (percept_pval indicator))
(ag_percept (percept_pobj ind1) (percept_pname partof) (percept_pval ev1))
(ag_percept (percept_pobj ind1) (percept_pname type) (percept_pval drum_prioritar))
(ag_percept (percept_pobj ind1) (percept_pname direction) (percept_pval inainte))
(ag_percept (percept_pobj ind1) (percept_pname proximity) (percept_pval 0))


(ag_percept (percept_pobj drum1) (percept_pname isa) (percept_pval drum))
(ag_percept (percept_pobj drum1) (percept_pname partof) (percept_pval ev1))
(ag_percept (percept_pobj drum1) (percept_pname direction) (percept_pval transversal))
(ag_percept (percept_pobj drum1) (percept_pname proximity) (percept_pval 1))


(ag_percept (percept_pobj masina2) (percept_pname isa) (percept_pval masina))
(ag_percept (percept_pobj masina2) (percept_pname partof) (percept_pval e1))
(ag_percept (percept_pobj masina2) (percept_pname on) (percept_pval drum1))
(ag_percept (percept_pobj masina2) (percept_pname intention) (percept_pval inainte))
